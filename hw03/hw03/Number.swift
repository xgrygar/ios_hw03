//
//  Number.swift
//  hw03
//
//  Created by Adam Grygar on 16/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import Foundation
import UIKit

class Number {
    var number : Int = 0
    var color = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
}
