//
//  DrawController.swift
//  hw03
//
//  Created by Adam Grygar on 16/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import UIKit
import Alamofire

class DrawController: UIViewController {
    weak var numbersListDelegate: CollectionViewDelegate?
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.stopAnimating()
    }
    
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    
    private func generate(color: UIColor, from: Int, to: Int) {
        let number = Number()
        loadingIndicator.startAnimating()
        redButton.isEnabled = false
        blueButton.isEnabled = false
        greenButton.isEnabled = false
        AF.request("https://www.random.org/integers/?num=1&min=\(from)&max=\(to)&col=1&base=10&format=plain&rnd=new").responseString { (response) in
            switch response.result {
            case .success(_):
                if let data = response.value{
                    let random_number : String = String(data[data.index(data.startIndex, offsetBy: 0) ..< data.index(data.endIndex, offsetBy: -1)])
                    
                    number.number = Int(random_number) ?? 0
                    number.color = color
                    
                    self.numbersListDelegate?.add(number: number)
                    self.loadingIndicator.stopAnimating()
                    self.redButton.isEnabled = true
                    self.blueButton.isEnabled = true
                    self.greenButton.isEnabled = true
                    self.dismiss(animated: true)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func animateButton(button : UIButton) {
        let original_bounds = button.bounds
        let original_transform = button.transform
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5, options: .curveEaseInOut,
                       animations: {
                        button.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        button.bounds = CGRect(x: button.bounds.origin.x - 20, y: button.bounds.origin.y, width: button.bounds.size.width, height: button.bounds.size.height)
        }, completion: {(succes: Bool) in
            if (succes){
                button.bounds = original_bounds
                button.transform = original_transform
            }
        })
    }
    
    @IBAction func drawRed(_ sender: UIButton) {
        animateButton(button: sender)
        generate(color: #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), from: 1, to: 10)
    }
    @IBAction func drawBlue(_ sender: UIButton) {
        animateButton(button: sender)
        generate(color: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), from: 11, to: 49)
    }
    @IBAction func drawGreen(_ sender: UIButton) {
        animateButton(button: sender)
        generate(color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), from: 11, to: 50)
    }
}
