//
//  CollectionViewController.swift
//  hw03
//
//  Created by Adam Grygar on 16/03/2020.
//  Copyright © 2020 FI MUNI. All rights reserved.
//

import UIKit

protocol CollectionViewDelegate: class {
    func add(number: Number)
}


private let ADD_NUMBER_SEGUE_ID = "addNumberSegue"
private let CELL_REUSE_ID = "collectionCell"

class CollectionViewController : UIViewController {
   
    private var numbers = [Number]()
    
    @IBOutlet weak var numbersCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numbersCollectionView.dataSource = self
        numbersCollectionView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if
            segue.identifier == ADD_NUMBER_SEGUE_ID,
            let addNumberController = segue.destination as? DrawController
        {
            addNumberController.numbersListDelegate = self
        }
    }
    
    private func removeNumber(at indexPath: IndexPath) {
        numbers.remove(at: indexPath.item)
        numbersCollectionView.deleteItems(at: [indexPath])
    }
    
}

extension CollectionViewController: CollectionViewDelegate {
    func add(number: Number) {
        numbers.append(number)
        numbersCollectionView.reloadData()
    }
}

extension CollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.removeNumber(at: indexPath)
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numbers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_REUSE_ID, for: indexPath) as? ItemCell else {
                  return UICollectionViewCell()
        }

        let item = numbers[indexPath.item]
        cell.numberLabel.text = "\(item.number)"
        cell.backgroundColor = item.color
        return cell
    }
}


